# BootCamp-Flask

Проект на GitLab: https://gitlab.com/darkfess/bootcamp-flask

Flask app for Python BootCamp (P1). Большое домашнее задание по теме "Flask": 21-22-23.

<br />

- [BootCamp-Flask](#bootcamp-flask)
  - [Запуск](#запуск)
  - [Описание](#описание)
  - [Тестирование](#тестирование)
  - [Источники](#источники)

<br />

## Запуск
- отредактируйте путь к приложению `__init.py__`
- запуск через `run.py`
<br /><br />

## Описание
При запущенном приложении, в веб-интерфейсе будут доступны материалы с описанием системы:
<br />
- основное окно, readme: http://127.0.0.1:5000/ <br />
- постановка задачи (ДЗ): http://127.0.0.1:5000/homework <br />
- спецификация API: http://127.0.0.1:5000/api <br />
- запросы через CLI с помощью curl: http://127.0.0.1:5000/cheatsheet <br />
<br />

## Тестирование
- для всех доступных методов API написаны юнит-тесты. Они находятся в `test.py` (не забудьте запустить само приложение перед запуском тестов)
- в [cheatsheet](http://127.0.0.1:5000/cheatsheet) доступна шпаргалка со списком команд, для взамодействия/тестирования API через CLI (curl)
<br /><br />

## Источники
- https://geekflare.com/securing-flask-api-with-jwt/
- https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/
- https://pythonworld.ru/moduli/modul-unittest.html
- https://stackoverflow.com/questions/tagged/python

