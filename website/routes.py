from flask import Flask, render_template, url_for, flash, redirect, request, make_response, jsonify
from flask_login import login_user, logout_user, current_user, login_required
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from website.forms import RegistrationForm, LoginForm
from website import app, db, bcrypt
from website.models import User, Item
import jwt
# pip install jwt pyjwt
import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps


links = []

@app.route("/")
@app.route("/home")
def home():
    return render_template("index.html", links=links)

@app.route("/api")
def api():
    return render_template("api.html", title="API-spec")

@app.route("/cheatsheet")
def cheatsheet():
    return render_template("cheatsheet.html", title="Cheat-Sheet")

@app.route("/README.md")
def readme():
    return render_template("README.md", title="ReadME")

@app.route("/homework")
def homework():
    return render_template("homework.html", title="HomeWork")


##### КОД с занятия, веб-интерфейс


@app.route("/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        hash_pass = bcrypt.generate_password_hash(form.password.data)
        user = User(
            username=form.username.data, email=form.email.data, password=hash_pass
        )
        db.session.add(user)
        db.session.commit()
        flash("You have been registrated", "success")
        return redirect(url_for("home"))
    return render_template("register.html", form=form)


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            next_page = request.args.get("next")
            print(next_page)
            return (
                redirect(url_for(next_page[1::]))
                if next_page
                else redirect(url_for("home"))
            )
        else:
            flash("Wrong credentials", "error")
    return render_template("login.html", form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("home"))


@app.route("/account")
@login_required
def account():
    return render_template("account.html")


registration_schema = {
    'type': 'object',
    'properties': {
        'login': {'type': 'string',  "minLength": 2, "maxLength": 20, "pattern": "^[\w.-]+$"},
        'password': {'type': 'string', "minLength": 4, "maxLength": 15}
    },
    'required': ['login', 'password']
}


##### API начинается здесь #####


## отдельный функционал для работы с токеном
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
       token = None

       if 'x-access-tokens' in request.headers:
          token = request.headers['x-access-tokens']

       if not token:
          return jsonify({'message': 'a valid token is missing'})

       try:
          data = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
          current_user = User.query.filter_by(id=data['id']).first() 
       except:  
          return jsonify({'message': 'token is invalid'})

       return f(current_user, *args,  **kwargs)
    return decorator


### curl -i -H "Content-Type: application/json" -X POST -d "{\"username\":\"tester1\",\"email\":\"tester1@darkfess.ru\",\"password\":\"tester1\"}" http://127.0.0.1:5000/api/registration
## регистрация нового пользователя, проверка наличие другого пользователя с таким же именем (если уже есть, то иди гуляй)
@app.route('/api/registration', methods = ['GET', 'POST'])
def registration():
    data = request.get_json()
    user_check = User.query.filter_by(username=data['username']).count()

# проверяем, нету ли дублей по имени пользователя
    if user_check != 0:
        return jsonify({'message': 'User already exists!'})

    hashed_password = generate_password_hash(data['password'], method='sha256')

    # user = User(username=data['username'], email=data['email'], password=data['password'])
    user = User(username=data['username'], email=data['email'], password=hashed_password)
    db.session.add(user)
    db.session.commit() 
    return jsonify({'message': 'Registration successful!'})


### curl -u tester1:tester1 http://127.0.0.1:5000/api/login
## авторизация пользователя, получение токена (на сутки)
@app.route('/api/login', methods=['GET', 'POST'])
def loginizer():
   auth = request.authorization

# проверка авторизации
   if not auth or not auth.username or not auth.password:
      return make_response('Could not verify...', 401, {'WWW.Authentication': 'Basic realm: "login required"'})

   user = User.query.filter_by(username=auth.username).first()

# генерация токена
   if check_password_hash(user.password, auth.password):
      token = jwt.encode(
          {'id': user.id, 
          'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=24)}, 
          app.config['SECRET_KEY'], 
          algorithm="HS256")
      return jsonify({'token' : token})

   return make_response('Could not verify...',  401, {'WWW.Authentication': 'Basic realm: "login required"'})


### curl -i http://127.0.0.1:5000/api/users
## получение общего списка пользователей, их id их почты (можно и с паролями, но это не правильно.. тем более без авторизации под админом и т.д.)
@app.route('/api/users', methods=['GET'])
def get_allusers():
   users = User.query.all()
   result = []

   for user in users:
      user_data = {}
      user_data['user_id'] = user.id
      user_data['username'] = user.username
      user_data['email'] = user.email
    #   user_data['password'] = user.password
      result.append(user_data)

   return jsonify({'users': result})


### set token=TOKEN
### curl -i -H "Content-Type: application/json" -H "x-access-tokens: %token%" -X POST http://127.0.0.1:5000/api/currentuser
## проверка: под каким мы сейчас пользователем находится (по токену), очень полезная штука когда надо что-то проверить
@app.route('/api/currentuser', methods=['POST', 'GET'])
@token_required
def check_current(current_user):
    user_check = User.query.filter_by(id=current_user.id).first()
    return jsonify({'message': user_check.username})


### curl -H "x-access-tokens: TOKEN" http://127.0.0.1:5000/api/items
## получение списка объектов (items, можно получить только с авторизацией по токену), их id и id-пользователя их создавших
@app.route('/api/items', methods=['GET', 'POST'])
@token_required
def items_get(current_user):
   items = Item.query.all()
   output = []

   for item in items:   
      item_data = {}  
      item_data['id'] = item.id
      item_data['name'] = item.name
      item_data['user_id'] = item.user_id
      output.append(item_data)  
   return jsonify({'list_of_items' : output})


### set token=TOKEN
### set name=NAME
### curl -i -H "Content-Type: application/json" -H "x-access-tokens: %token%" -X POST -d "{\"name\":\"%name%\"}" http://127.0.0.1:5000/api/additem
## добавление объектов (items, и это можно сделать только авторизовавшись по токену) по имени (name), проверка наличия другого объекта с таким же именем (без дубликатов)
@app.route('/api/additem', methods=['POST', 'GET'])
@token_required
def item_add(current_user):
    data = request.get_json()
    item_check = Item.query.filter_by(name=data['name']).count()

# проверка: нашлись ли дубли
    if item_check != 0:
        return jsonify({'message': 'Item already exists!'})

# создаем
    new_items = Item(name=data['name'], user_id=current_user.id)

    db.session.add(new_items)
    db.session.commit()
    return jsonify({'message' : 'New item created!'})


### set token=TOKEN
### set name=NAME
### curl -i -H "Content-Type: application/json" -H "x-access-tokens: %token%" -X DELETE -d "{\"name\":\"%name%\"}" http://127.0.0.1:5000/api/delitem
## удаление объектов (items, и это можно сделать только авторизовавшись по токену) по имени (name), проверка наличия объекта с таким именем перед выполнением операции
@app.route('/api/delitem', methods=['DELETE'])
@token_required
def item_delete(current_user):
    data = request.get_json()
    item_check = Item.query.filter_by(name=data['name']).count()

# проверка: нашлись ли объекты с таким же именем
    if item_check == 0:
        return jsonify({'message': 'No such item!'})

# удаляем 
    del_items = Item.query.filter_by(name=data['name']).first()

    db.session.delete(del_items)
    db.session.commit()
    return jsonify({'message': 'Item deleted!'})


### set token=TOKEN
### set name=NAME
### set receiver=RECEIVER
### curl -i -H "Content-Type: application/json" -H "x-access-tokens: %token%" -X POST -d "{\"name\":\"%name%\",\"receiver_name\":\"%receiver%\"}" http://127.0.0.1:5000/api/send
@app.route('/api/send', methods=['POST', 'GET'])
@token_required
def item_send(current_user):
    data = request.get_json()
    
# делая запрос в БД, определяем id пользователя приславшего запрос (по токену)
    user_check = User.query.filter_by(id=current_user.id).first()

# делаем запрос в БД, определяем user_id пользователя создавшего item (по имени item)
    item_check = Item.query.filter_by(name=data['name']).first()

# проверка: сравниваем id пользователя (который отправил запрос со своим токеном) с базы User и user_id пользователя (создавшего данный item) с базы Item
    if user_check.id != item_check.user_id:
        return jsonify({'message' : "Not your item!"})

    item_name = data['name']
    receiver_name = data['receiver_name']

# генерируем отдельный token для передачи item конкретному пользователю (по username)
    item_token = jwt.encode(
        {'item' : item_name,
        'receiver_name': receiver_name},
        app.config['SECRET_KEY'],
        algorithm="HS256")

    return jsonify({'item_token' : item_token})


### set token=TOKEN
### set name=NAME
### set tokenit=ITEMTOKEN
## curl -i -H "Content-Type: application/json" -H "x-access-tokens: %token%" -X POST -d "{\"tokenit\":\"%tokenit%\"}" http://127.0.0.1:5000/api/get
@app.route('/api/get', methods=['POST', 'GET'])
@token_required
def item_get(current_user):
    data = request.get_json()

# получаем-декодируем содержимое токена
    item_data = jwt.decode(
        data['tokenit'], 
        app.config['SECRET_KEY'], 
        algorithms="HS256")

# получаем имя получателя (username)
    receiver_name=item_data['receiver_name']

# делаем запросы в БД User и Item, получаем данные о текущем авторизованном пользователе и отправленном item (данные об передаваемом item мы получаем расшифровав токен)
    user_check = User.query.filter_by(id=current_user.id).first()
    item_check = Item.query.filter_by(name=item_data['item']).first()

# проверка: сравниваем имя (username) пользователя (который отправил запрос со своим токеном) с именем получателя, что указан в токене
    if user_check.username != receiver_name:
        return jsonify({'message' : "Incorrect receiver!"})

# меняем владельца item и применяем изменения
    item_check.user_id = user_check.id
    db.session.commit()
    return jsonify({'message': 'Item transfer successfull!'})


### ДЕБАГ
    # return jsonify(user_check.id)
# print()
    # return jsonify(item_check.user_id)
# print()