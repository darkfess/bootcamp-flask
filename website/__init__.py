from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
import os


app = Flask(__name__)
app.config["SECRET_KEY"] = '2688088b53e6e0116a1ddb59f72db9d1'
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///website.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]  = False
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.login_message_category = "error"


### ПОМЕНЯЙ МЕНЯ... ЛЕЖУ ГДЕ ХОЧУ или "ТАКОВ ПУТЬ" (с)
os.chdir('X:\\YandexDisk\\Мануалы\\python\\23 [24.08.2021]\\dfhw21-23')


from website import routes


### ОБСЛУЖИВАНИЕ (db cleanup, init)... можно, но не обязательно
# db.drop_all()
# db.create_all()