### перед запуском тестов, запустите само приложение приложение (через run.py)
### адрес http://127.0.0.1:5000/ должен быть доступен

import json
import requests
from requests.auth import HTTPBasicAuth
import unittest
import random


class BootCampFlask(unittest.TestCase):

# регистрация нового пользователя
    def test_registration_valid(self):
        # создадим пользователя со случайным именем (tester100-tester1000), иначе будет ругаться на повторяющиеся имена... пароль и почта
        number = random.randrange(100, 10000)
        name = "tester"
        user = ("{}{}".format(name, number))
        email = ("{}{}{}".format(name, number, '@darkfess.ru'))
        # сохраняем сгенерированный username для проверки в дальнейшем
        with open("DF_user_name.txt", "w", encoding="utf-8") as f:
            f.write(user)
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/registration'
        head = {'Content-Type': 'application/json'}
        payload = {"username": user, "email":email, "password":user}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Registration successful!"}'
        self.assertEqual(self.answer, remove_BR)

# попытка регистрации пользователя с уже занятым именем (сообщение: такое имя пользователя уже занято)
    def test_registration_invalid(self):
        # указываем системного тестового пользователя tester1, который точно есть в БД
        user = "tester1"
        url = 'http://127.0.0.1:5000/api/registration'
        head = {'Content-Type': 'application/json'}
        payload = {"username": user, "email":"tester@darkfess.ru", "password":user}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "User already exists!"}'
        self.assertEqual(self.answer, remove_BR)

# получение списка пользователей
    def test_user_list(self):
        url = 'http://127.0.0.1:5000/api/users'
        response = requests.get(url)
        self.answer = response.ok
        self.assertEqual(self.answer, True)

# проверка авторизации, логинимся под пользователем и сохраняем его токен в файл
    def test_user_login(self):
        # входим в систему с пользователем, сгенерированным в тесте test_registration_valid
        with open("DF_user_name.txt", "r", encoding="utf-8") as f:
            user = f.read()
        url = 'http://127.0.0.1:5000/api/login'
        response = requests.get(url, auth=HTTPBasicAuth(user, user))
        # аккуратно чистим вывод, удаляем все лишнее - нам нужен только token, без единого лишнего символа (парсинг)
        clean = response.text
        remove_BRUP = clean.replace("{", "")
        remove_BRDOWN = remove_BRUP.replace("}", "")
        remove_TOKEN = remove_BRDOWN.replace('"token":', "")
        remove_QT = remove_TOKEN.replace('"', '')
        remove_SPACE = remove_QT.replace('   ', '')
        no_empty_LINES = remove_SPACE.strip()
        # сохраняем полученный токен в текстовый файл
        with open("DF_user_token.txt", "w", encoding="utf-8") as f:
            f.write(no_empty_LINES)
        # проверяем токен, длинна нашего токена по алгоритму (jwt/HS256) составляет где то между 115 и 120 символов (зависит от длинны сгенерированной цифровой приставки в имени user, tester100 - tester10000)
        self.size = len(no_empty_LINES)
        self.assertTrue(115 <= self.size <= 120)

# проверка под каким именно пользователем мы сейчас залогинены (под его токеном)
    def test_current_username(self):
        # считываем имя пользователя (для проверки), полученное в тесте test_registration_valid
        with open("DF_user_name.txt", "r", encoding="utf-8") as f:
            user = f.read()
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        url = 'http://127.0.0.1:5000/api/currentuser'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        response = requests.post(url, headers=head)
        # аккуратно чистим вывод, удаляем все лишнее - нам нужен только user, без единого лишнего символа (парсинг)
        clean = response.text
        remove_BRUP = clean.replace("{", "")
        remove_BRDOWN = remove_BRUP.replace("}", "")
        remove_TOKEN = remove_BRDOWN.replace('"message":', "")
        remove_QT = remove_TOKEN.replace('"', '')
        remove_SPACE = remove_QT.replace('   ', '')
        no_empty_LINES = remove_SPACE.strip()
        # проверяем пользователя, сравниваем имя пользователя созданного в тесте test_registration_valid и записанного в файл, с ответом от API
        self.username = no_empty_LINES
        self.assertEqual(self.username, user)

# получение списка текущих объектов и информации по ним
    def test_item_list(self):
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        url = 'http://127.0.0.1:5000/api/items'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        response = requests.post(url, headers=head)
        self.answer = response.ok
        self.assertEqual(self.answer, True)

# создание нового item
    def test_item_add_valid(self):
        # создадим название item со случайным именем (test-item-10000 - test-item-100000), иначе будет ругаться на повторяющиеся имена
        number = random.randrange(100, 10000)
        item = "test-item-"
        name = ("{}{}".format(item, number))
        # сохраняем название item в файл, чтобы в дальнейших тестах с ним работать
        with open("DF_item_name.txt", "w", encoding="utf-8") as f:
            f.write(name)
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/additem'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "New item created!"}'
        self.assertEqual(self.answer, remove_BR)

# попытка создания item с уже занятым именем
    def test_item_add_invalid(self):
        # укажем название item которое уже занято
        name = "test-item-1"
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/additem'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Item already exists!"}'
        self.assertEqual(self.answer, remove_BR)

# удаляем созданный item
    def test_item_del_valid(self):
        # используем имя item (этот item мы будем удалять), полученным в тесте test_item_add_valid
        with open("DF_item_name.txt", "r", encoding="utf-8") as f:
            name = f.read()
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/delitem'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name}
        response = requests.delete(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Item deleted!"}'
        self.assertEqual(self.answer, remove_BR)

# пробуем удалить item с несуществующим именем
    def test_item_del_invalid(self):
        # используем имя item, которого точно нет
        name = 'test-item-9999'
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/delitem'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name}
        response = requests.delete(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "No such item!"}'
        self.assertEqual(self.answer, remove_BR)

# создаем новый item, который мы будем отправлять (а не удалять, как первый) другому пользователю
    def test_item_add_for_send_get(self):
        # создадим еще один item (для дальнейшего send/get)
        # создадим название item со случайным именем (test-item-10000 - test-item-100000), иначе будет ругаться на повторяющиеся имена
        number = random.randrange(100, 10000)
        item = "test-item-"
        name = ("{}{}".format(item, number))
        # сохраняем название item в файл, чтобы в дальнейших тестах с ним работать
        with open("DF_item_name_send.txt", "w", encoding="utf-8") as f:
            f.write(name)
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/additem'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "New item created!"}'
        self.assertEqual(self.answer, remove_BR)

# входим в систему под tester1, получаем и сохраняем токен
    def test_login_tester1(self):
        # входим в систему с системным тестовым пользователем tester1, который точно есть в БД
        user = "tester1"
        url = 'http://127.0.0.1:5000/api/login'
        response = requests.get(url, auth=HTTPBasicAuth(user, user))
        # аккуратно чистим вывод, удаляем все лишнее - нам нужен только token, без единого лишнего символа (парсинг)
        clean = response.text
        remove_BRUP = clean.replace("{", "")
        remove_BRDOWN = remove_BRUP.replace("}", "")
        remove_TOKEN = remove_BRDOWN.replace('"token":', "")
        remove_QT = remove_TOKEN.replace('"', '')
        remove_SPACE = remove_QT.replace('   ', '')
        no_empty_LINES = remove_SPACE.strip()
        # сохраняем полученный токен в текстовый файл
        with open("DF_tester1_token.txt", "w", encoding="utf-8") as f:
            f.write(no_empty_LINES)
        # проверяем токен, длинна нашего токена по алгоритму (jwt/HS256) составляет где то между 115 и 120 символов (зависит от длинны сгенерированной цифровой приставки в имени user, tester100 - tester10000)
        self.size = len(no_empty_LINES)
        self.assertTrue(115 <= self.size <= 120)

# входим в систему под tester2, получаем и сохраняем токен
    def test_login_tester2(self):
        # входим в систему с системным тестовым пользователем tester2, который точно есть в БД
        user = "tester2"
        url = 'http://127.0.0.1:5000/api/login'
        response = requests.get(url, auth=HTTPBasicAuth(user, user))
        # аккуратно чистим вывод, удаляем все лишнее - нам нужен только token, без единого лишнего символа (парсинг)
        clean = response.text
        remove_BRUP = clean.replace("{", "")
        remove_BRDOWN = remove_BRUP.replace("}", "")
        remove_TOKEN = remove_BRDOWN.replace('"token":', "")
        remove_QT = remove_TOKEN.replace('"', '')
        remove_SPACE = remove_QT.replace('   ', '')
        no_empty_LINES = remove_SPACE.strip()
        # сохраняем полученный токен в текстовый файл
        with open("DF_tester2_token.txt", "w", encoding="utf-8") as f:
            f.write(no_empty_LINES)
        # проверяем токен, длинна нашего токена по алгоритму (jwt/HS256) составляет где то между 115 и 120 символов (зависит от длинны сгенерированной цифровой приставки в имени user, tester100 - tester10000)
        self.size = len(no_empty_LINES)
        self.assertTrue(115 <= self.size <= 120)

# отправляем созданный item другому пользователю
    def test_item_send_valid(self):
        # считываем имя item (которое нам нужно будет передать), полученное в тесте test_item_add_for_send_get
        with open("DF_item_name_send.txt", "r", encoding="utf-8") as f:
            name = f.read()
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_user_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # получателем указываем системного тестового пользователя tester1, который точно есть в БД
        receiver = "tester1"
        url = 'http://127.0.0.1:5000/api/send'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name, "receiver_name": receiver}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # аккуратно чистим вывод, удаляем все лишнее - нам нужен только token, без единого лишнего символа (парсинг)
        clean = response.text
        remove_BRUP = clean.replace("{", "")
        remove_BRDOWN = remove_BRUP.replace("}", "")
        remove_TOKEN = remove_BRDOWN.replace('"item_token":', "")
        remove_QT = remove_TOKEN.replace('"', '')
        remove_SPACE = remove_QT.replace('   ', '')
        no_empty_LINES = remove_SPACE.strip()
        # сохраняем полученный item-token в текстовый файл, для дальнейшем работы сн им
        with open("DF_item_token_send.txt", "w", encoding="utf-8") as f:
            f.write(no_empty_LINES)
        # проверяем токен, длинна нашего токена по алгоритму (jwt/HS256) составляет где то между 146 и 153 символов (зависит от длинны сгенерированной цифровой приставки в имени item, test-item-100 - test-item-10000)
        self.size = len(no_empty_LINES)
        self.assertTrue(146 <= self.size <= 153)

# пытаемся отправить не нами созданный item другому пользователю
    def test_item_send_invalid(self):
        # указываем системный тестовый item: test-item-1 
        name = "test-item-1"
        # входим в систему с токеном, полученным в тесте test_login_tester2
        with open("DF_tester2_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        receiver = "tester1"
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/send'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name, "receiver_name": receiver}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Not your item!"}'
        self.assertEqual(self.answer, remove_BR)

# получаем отправленный нам item
    def test_item_get_valid(self):
        # указываем token отправленного item в тесте test_item_send_valid
        with open("DF_item_token_send.txt", "r", encoding="utf-8") as f:
            tokenit = f.read()
        # авторизуемся по token системного тестового пользователя tester1 (именно ему мы отправили данный item)
        with open("DF_tester1_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/get'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"tokenit": tokenit}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Item transfer successfull!"}'
        self.assertEqual(self.answer, remove_BR)

# пытаемся получить чужой (отправленный не нам) item
    def test_item_get_invalid(self):
        # указываем token отправленного item в тесте test_item_send_valid
        with open("DF_item_token_send.txt", "r", encoding="utf-8") as f:
            tokenit = f.read()
        # авторизуемся по token системного тестового пользователя tester2 (ему мы данный item НЕ отправляли)
        with open("DF_tester2_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/get'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"tokenit": tokenit}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Incorrect receiver!"}'
        self.assertEqual(self.answer, remove_BR)

### Отключенное. Включать только для восстановления утерянных системных объектов!

# на случай если исходная база потерта! создает системного тестового пользователя tester1 (который используется некоторыми тестами ниже)
    @unittest.skip
    def test_create_basic_tester1(self):
        # простые параметры авторизации, системного тестового пользователя tester1
        user = "tester1"
        email = "tester1@darkfess.ru"
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/registration'
        head = {'Content-Type': 'application/json'}
        payload = {"username": user, "email":email, "password":user}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Registration successful!"}'
        self.assertEqual(self.answer, remove_BR)

# на случай если база потерта! создает системного тестового пользователя tester1 (который используется некоторыми тестами ниже)
    @unittest.skip
    def test_create_basic_tester2(self):
        # простые параметры авторизации
        user = "tester2"
        email = "tester2@darkfess.ru"
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/registration'
        head = {'Content-Type': 'application/json'}
        payload = {"username": user, "email":email, "password":user}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "Registration successful!"}'
        self.assertEqual(self.answer, remove_BR)

# на случай если база потерта! создает системного тестового пользователя test-item-1 (который используется некоторыми тестами ниже)
    @unittest.skip
    def test_add_basic_testitem1(self):
        # создадим системный item: test-item-1 под системным тестовым пользователем tester1
        name = "test-item-1"
        # входим в систему с токеном, полученным в тесте test_user_login
        with open("DF_tester1_token.txt", "r", encoding="utf-8") as f:
            token = f.read()
        # формируем и отправляем запрос
        url = 'http://127.0.0.1:5000/api/additem'
        head = {'Content-Type': 'application/json', 'x-access-tokens': token}
        payload = {"name": name}
        response = requests.post(url, data=json.dumps(payload), headers=head)
        # чистим полученный вывод (парсинг)
        clean = response.text
        remove_N = clean.replace("\n", "")
        remove_BR = remove_N.replace("{  ", "")
        # сравниваем, смотрим результат
        self.answer = '"message": "New item created!"}'
        self.assertEqual(self.answer, remove_BR)

if __name__ == "__main__":
    unittest.main()